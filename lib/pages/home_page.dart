import 'package:dclost_app/pages/join_page.dart';
import 'package:dclost_app/pages/login_id_find_page.dart';
import 'package:dclost_app/pages/login_page.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            padding: EdgeInsets.all(60),
            margin:EdgeInsets.all(20) ,
          child: Column(
            children: [
            Text(
                style: TextStyle(fontSize: 55, fontWeight: FontWeight.bold,fontFamily: "Serif", ),'Dcloset'),
            Container(
              padding: EdgeInsets.all(50),
             margin: EdgeInsets.all(20),
             child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  padding: EdgeInsets.all(5),
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => LoginPage()));
                    },
                    style: ElevatedButton.styleFrom( //엘리베이터 버튼 스타일
                      backgroundColor: Colors.black,
                      foregroundColor: Colors.white,
                    ),
                    child: Text('로그인'),
                  ),
                ),
                Container(

                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => LoginIdFind()));
                      }, child: Text('아이디 찾기'),
                      ),
                      Container(
                        child: Column(
                          children: [
                            TextButton(onPressed: () {}, child: Text('비밀번호 찾기'),
                            ),
                          ],
                        ),
                      ),
                    ],
                    ),
                  ),
                Container(
                  margin: EdgeInsets.all(5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                          style: TextStyle(fontSize: 11,fontWeight: FontWeight.bold),'아직 디클로젯의 회원이 아니시라면 ?'),
                     ],
                    ),
                  ),
                Container(
                  margin: EdgeInsets.all(5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => JoinPage())); // 회원가입 누르면 join 페이지로 이동
                      }, child: Text('회원가입')
                      ),
                     ],
                    ),
                  ),
                ],
              ),
            ),
         ],
        ),
      )
    );
  }
}
