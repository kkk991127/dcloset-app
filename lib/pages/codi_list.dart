import 'package:flutter/material.dart';

class CodiList extends StatefulWidget {
  const CodiList({super.key});

  @override
  State<CodiList> createState() => _CodiListState();

}

class _CodiListState extends State<CodiList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('코디 리스트'),
        centerTitle: true,
      ),
      body: GridView.count(
        crossAxisCount: 2, // 1개 행에 항목 3개씩
        mainAxisSpacing: 5,
        crossAxisSpacing: 5,
        childAspectRatio: 1/2,
        children: List.generate(
          4, // 항목 개수
              (int index) {
            return Card(
              elevation: 2,
              child: Column(
                children: <Widget>[
                  Expanded(child: Image.asset('assets/codi1.png',width: 200, height: 200,),flex: 5,
                  ),
                  Expanded(child: ListTile(
                   title: Text('꾸안꾸 코디세트'),
                    subtitle: Text('25,000원'),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

