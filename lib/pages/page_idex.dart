import 'package:flutter/material.dart';

/**
 * 메인 페이지
 */
class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _bottomSelectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar( //하단 바
        currentIndex: _bottomSelectedIndex,
        onTap: (index){setState(() {
          _bottomSelectedIndex = index;
        });},

        type: BottomNavigationBarType.fixed, // 하단 바 스타일
        selectedItemColor: Colors.black87,
        unselectedItemColor: Colors.black38,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.arrow_circle_left), label: '뒤로가기'),
          BottomNavigationBarItem(icon: Icon(Icons.home), label: '홈'),
          BottomNavigationBarItem(icon: Icon(Icons.search),label: '검색'),
          BottomNavigationBarItem(icon: Icon(Icons.menu),label: '메뉴'),
        ],
      ),
      appBar: AppBar(
        title: Image.asset('assets/logo.png'),
        centerTitle: true,
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              print('shopping cart button is clicked');
            },
          ),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              print('Search button is clicked');
            },
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/profile1.png'),
                backgroundColor: Colors.white,
              ),
              otherAccountsPictures: <Widget>[
                // 다른 계정 이미지[] set
                CircleAvatar(
                  backgroundColor: Colors.white,
                  backgroundImage: AssetImage('assets/profile2.png'),
                ),
              ],
              accountName: Text('SeungHee'),
              accountEmail: Text('seunghee@email.com'),
              onDetailsPressed: () {
                print('arrow is clicked');
              },
              decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(40.0),
                      bottomRight: Radius.circular(40.0))),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
                color: Colors.grey[850],
              ),
              title: Text('Home'),
              onTap: () {
                print('Home is clicked');
              },
              trailing: Icon(Icons.add),
            ),
            ListTile(
              leading: Icon(
                Icons.settings,
                color: Colors.grey[850],
              ),
              title: Text('Setting'),
              onTap: () {
                print('Setting is clicked');
              },
              trailing: Icon(Icons.add),
            ),
            ListTile(
              leading: Icon(
                Icons.question_answer,
                color: Colors.grey[850],
              ),
              title: Text('Q&A'),
              onTap: () {
                print('Q&A is clicked');
              },
              trailing: Icon(Icons.add),
            ),
          ],
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.all(5),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                Text(
                    style: TextStyle(fontSize: 15,fontStyle: FontStyle.italic, ),'"당신만의 놀라운 Dcloset"'),
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Image.asset('assets/main2.png',width: 450, height: 430,fit: BoxFit.fill,),
                      Container(
                        padding: EdgeInsets.all(10),
                        margin: EdgeInsets.all(10),
                        child: Column(
                          children: [
                            Text(
                                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),'Dcloset Cataloge')
                          ],
                        )
                      )
                    ],
                    ),
                  ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/codi1.png',width: 100, height: 100,),
                      Image.asset('assets/codi2.png',width: 100, height: 100,),
                      Image.asset('assets/codi3.png',width: 100, height: 100,),
                      Image.asset('assets/codi4.png',width: 100, height: 100,),
                    ],
                  ),
                )


                ],
              ),
            ),
          ),
        ),
      );
    }
  }

