import 'package:flutter/material.dart';

/**
 * 가입하기 페이지
 */

class JoinPage extends StatefulWidget {

  @override
  State<JoinPage> createState() => _JoinPageState();
}

class _JoinPageState extends State<JoinPage> {
  TextStyle style = TextStyle(fontSize: 20.0);
  final _formKey = GlobalKey<FormState>();
  final _key = GlobalKey<ScaffoldState>();
  var _isChecked = false;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      appBar: AppBar(
        title: Image.asset('assets/logo.png'),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Form(
        key: _formKey,
        child: Center(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(5.0),
              child: TextFormField(
                style: style,
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.supervised_user_circle),
                  labelText: "아이디",
                  border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  style: style, // TextFormField 스타일 주기
                  decoration: InputDecoration(
                  prefixIcon: Icon(Icons.perm_identity_outlined),
                  labelText: "이름",
                  border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  obscureText: true, // 비밀번호 숫자 가려주는 옵션
                  style: style, // TextFormField 스타일 주기
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.lock),
                    labelText: "비밀번호",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  obscureText: true, // 비밀번호 숫자 가려주는 옵션
                  style: style, // TextFormField 스타일 주기
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.lock),
                    labelText: "비밀번호 확인",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  style: style,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.call), // 전화기 모양 아이콘
                    labelText: "연락처",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  style: style,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.map), // 지도 아이콘
                    labelText: "주소",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  style: style,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.map),
                    labelText: "상세주소",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Checkbox(value: _isChecked, onChanged: (value){
                      setState(() {
                        _isChecked = value!;
                      });
                    }),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('대여 소식 안내, 배송소식 등 문자 수신에 동의합니다.')
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: Column(
                  children: [
                    ElevatedButton(onPressed: (){},
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        foregroundColor: Colors.white,
                      ),
                      child: Text('가입하기'),),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: Column(
                  children: [
                    ElevatedButton(onPressed: (){},
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        foregroundColor: Colors.white,
                      ),
                      child: Text('취소'),),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
